$(document).ready(function () {
    var full = $(window).height();
    var full2 = $('.footer').height();
    full = full - full2;
    var doc = $('body').height();

    if (full > doc) {
        $('.footer').addClass('footer_fixed');
    } else {
        $('.footer').removeClass('footer_fixed');
    }
});


jQuery(function ($) {

    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});

wow = new WOW({
        mobile: false
    });
    wow.init();
   
   
});
