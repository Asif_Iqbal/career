<?php

	class Welcome_model extends CI_Model{

		function __construct(){
			parent::__construct();
		}

        function view_profile($id){
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('id', $id);
            $query = $this->db->get();

            return $query->row();
        }

        function field($id){
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('id', $id);
            $query = $this->db->get();

            $result = $query->result_array();

            foreach($result as $post){
                $cat_name = $post['user_field'];

                $this->db->select('*');
                $this->db->from('posts');
                $this->db->where('cat_name', $cat_name);
                $output = $this->db->get();

                return $output->result();
            } 
        }

        private function do_upload($value){
        $type = explode('.', $_FILES[$value]["name"]);
        $type = $type[count($type)-1];
        $url = "./assets/uploads/user_image/".uniqid(rand()).'.'.$type;
        if(in_array($type, array("jpg","jpeg","gif","png","mp4")))
            if(is_uploaded_file($_FILES[$value]["tmp_name"]))
                if(move_uploaded_file($_FILES[$value]["tmp_name"], $url))
                    return $url;
        return "";
    }

        function update_user_profile(){
            $id = $this->input->post('txt_hidden');
            $field = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'user_image' => $this->do_upload('user_image'),
                'interested_field' => $this->input->post('interested_field'),
                'user_field' => $this->input->post('user_field')
            );
            $this->db->where('id', $id);
            $this->db->update('users', $field);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        public function insert_data($data){
            return $this->db->insert('users_cv', $data);
        }

        public function get_cv($id){
            $this->db->select('*');
            $this->db->from('users_cv');
            $this->db->where('user_id', $id);
            $data = $this->db->get();
            return $data->result();

        }

		public function insert($data){
			return $this->db->insert('messages', $data);
		}

		public function fetch_sticker(){
			$this->db->select('*');
			$this->db->from('sticker');
			$data = $this->db->get();

			return $data->result();
		}

        function get_posts($num=20,$start=0){
            $this->db->select()->from('posts')->order_by('created_on')->limit($num, $start);;
            $query = $this->db->get();

            return $query->result();
        }

        function fetch_id($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            $this->db->from('posts');
            $query = $this->db->get();

            return $query->row();
        }

        function get_posts_count(){
            $this->db->select('id')->from('posts');
            $query = $this->db->get();

            return $query->num_rows();
        }

        

		function exam_result(){
			$this->db->select('*');
			$this->db->from('platform');
			$query = $this->db->get();

			return $query->result_array();
		}

		function fetch_comment($id){
			$this->db->select('*');
			$this->db->from('comments');
			$this->db->where('c_blog_id',$id);
			$result = $this->db->get();

			return $result->result();
		}

		function getUserName($uid){
			$this->db->select('*');
            $this->db->from('users');
            $this->db->where('id', $uid);
            $query = $this->db->get();

            if($query->num_rows() == 1 ){
                return $query->result_array();
            }else{
                return false;
            }
		}

		function getUserName2($uid){
			$this->db->select('*');
            $this->db->from('users');
            $this->db->where('id', $uid);
            $query = $this->db->get();

            if($query->num_rows() == 1 ){
                return $query->result();
            }else{
                return false;
            }
		}

		function comment($data){
            return $this->db->insert('comments', $data);
        }

        function edit_comment($p_id){

            $var = $this->dashboard_model->update_comment();
            if($var){
                redirect('welocme/posts/'.$p_id);
            } else{
                redirect('welocme/posts/'.$p_id);
            }
        }

        function delete_comment($c_id){
            $del = $this->dashboard_model->d_comment();
            if($del){
                redirect('welocme/posts/'.$c_id);
            } else{
                redirect('welocme/posts/'.$c_id);
            }
        }

        function update_comment(){
            $id = $this->input->post('c_id');
            $comment = $this->input->post('update_comment');
            $data = array('comment' => $comment);   
            $this->db->where('id', $id);
            $this->db->update('comments', $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function d_comment(){
            $id = $this->input->post('post_id');
            $this->db->where('id', $id);
            $this->db->delete('comments');
            if($this->db->affected_rows()>0){
                return true;
            } else{
                return false;
            }
        }

         function get_cat_name(){
        	$this->db->select('id,cat_name');
        	$query = $this->db->get('category');

        	return $query->result();
        }

        function get_sub_cat_name(){
            $this->db->select('id,sub_cat_name');
            $query = $this->db->get('sub_category');

            return $query->result();
        }

        function get_post_title(){
            $this->db->select('id,post_title');
            $query = $this->db->get('posts');

            return $query->result();
        }

		function fetch_category(){
			$this->db->select('*');
			$this->db->from('category');
			$fetch = $this->db->get();
			return $fetch->result();
		}

		function fetch_sub_category($category){
			$this->db->select('*');
			$this->db->where('cat_name', $category);
			$this->db->from('sub_category');
			$fetch = $this->db->get();

			return $fetch->result();
		}

		function fetch_sub_post($subCategory){
			$this->db->select('*');
            $this->db->where('sub_cat_name', $subCategory);
			$this->db->where('status', 'active');
			$this->db->from('posts');
			$fetch = $this->db->get();

			return $fetch->result();
		}

        function fetch_all_posts(){
            $this->db->select('*');
            $this->db->where('status', 'active');
            $this->db->from('posts');
            $fetch = $this->db->get();

            return $fetch->result();
        }

		function fetch_post($id){
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->where('status', 'active');
			$this->db->from('posts');
			$fetch = $this->db->get();

			return $fetch->result();
		}

        function fetch_rating($id){
            $this->db->select('*');
            $this->db->where('post_id', $id);
            $this->db->from('rating');
            $fetch = $this->db->get();

            return $fetch->result();
        }

        function userRating($user_id, $post_id, $rating){
            $this->db->select('*');
            $this->db->from('rating');
            $this->db->where("post_id", $post_id);
            $this->db->where("user_id", $user_id);
            $userRatingquery = $this->db->get();

            $userRatingResult = $userRatingquery->result_array();
            if(count($userRatingResult) > 0){

              $postRating_id = $userRatingResult[0]['id'];
              // Update
              $value=array('rating'=>$rating);
              $this->db->where('id',$postRating_id);
              $this->db->update('rating',$value);
            }else{
              $userRating = array(
                "post_id" => $post_id,
                "user_id" => $user_id,
                "rating" => $rating
              );

              $this->db->insert('rating', $userRating);
            }

            // Average rating
            $this->db->select('ROUND(AVG(rating),1) as averageRating');
            $this->db->from('rating');
            $this->db->where("post_id", $post_id);
            $ratingquery = $this->db->get();

            $postResult = $ratingquery->result_array();

            $rating = $postResult[0]['averageRating'];

            if($rating == ''){
               $rating = 0;
            }

            return $rating;
        }

		function get_search($keyword){
			$this->db->like('post_title',$keyword);
			$query = $this->db->get('posts');

			return $query->result();
		}

		function getPosts(){
            $query = $this->db->get('public_posts');
            
            return $query->result();
        }

		function countPosts(){
            return $this->db->count_all('public_posts');
        }

        function countUsers(){
            return $this->db->count_all('users');
        }

        function add_post_data(){
        	$data = array(
                'u_id' => $this->input->post('u_id'),
                'title' => $this->input->post('title'),
                'body' => $this->input->post('body')
            );

            return $this->db->insert('public_posts', $data);
        }

        function getBlogById($id){
        	$this->db->where('id', $id);
            $query = $this->db->get('public_posts');
            if($query->num_rows() > 0){
                return $query->row();
            } else{
                return false;
            }
        }

        function getComments($id){
        	$this->db->select('*');
            $this->db->where('c_blog_id', $id);
            $query = $this->db->get('public_comments');
            if($query->num_rows() > 0){
                return $query->result();
            } else{
                return false;
            }
        }

        function d_post($id){
        	$this->db->where('id', $id);
            $this->db->delete('public_posts');
            if($this->db->affected_rows()>0){
                return true;
            } else{
                return false;
            }
        }

        function update(){
        	$id = $this->input->post('text_hidden');
            $field = array(
                'title' => $this->input->post('title'),
                'body' => $this->input->post('body'),
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->db->where('id', $id);
            $this->db->update('public_posts', $field);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function blog_comment($data){
        	return $this->db->insert('public_comments', $data);
        }

        function blog_update_comment(){
            $id = $this->input->post('c_id');
            $comment = $this->input->post('update_comment');

            $data = array('comment' => $comment);
            
            $this->db->where('id', $id);
            $this->db->update('public_comments', $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function blog_d_comment(){
            $id = $this->input->post('post_id');

            $this->db->where('id', $id);
            $this->db->delete('public_comments');
            if($this->db->affected_rows()>0){
                return true;
            } else{
                return false;
            }
        }
	}

?>