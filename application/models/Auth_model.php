<?php

	class Auth_model extends CI_Model{

		public function __construct(){
			parent::__construct();
		}

		public function insert($data){
			return $this->db->insert('users', $data);
		}

		public function get($email, $password){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('email', $email);
			$this->db->where('password', md5($password));

			$query = $this->db->get();
			if($query->num_rows() == 1){
				return $query->result();
			} else{
				return false;
			}
		}

		
	}

?>