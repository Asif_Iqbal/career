<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('welcome_model'));
	}

	
	function index(){
		$data['sticker'] = $this->welcome_model->fetch_sticker();

		// $data['posts'] = $this->welcome_model->get_posts(1,$start);
		
		// $this->load->library('pagination');
		// $config['base_url'] = base_url().'welcome/index/';
		// $config['total_rows'] = $this->welcome_model->get_posts_count();
		// $config['per_page'] = 1;
		// $this->pagination->initialize($config);
		// $data['pages'] = $this->pagination->create_links();


		$this->load->view('includes/header');
		$this->load->view('public/home', $data);
		$this->load->view('includes/footer');
	}

	function view_profile($id){
		$profile['data'] = $this->welcome_model->view_profile($id);
		// echo "<pre>";
		// print_r($profile);
		// echo "</pre>"; exit();
		$this->load->view('includes/header');
		$this->load->view('public/profile', $profile);
		$this->load->view('includes/footer');
	}

	function your_field($id){
		$profile['data'] = $this->welcome_model->field($id);
		$profile['user'] = $this->welcome_model->view_profile($id);
		$this->load->view('includes/header');
		$this->load->view('public/users_field', $profile);
		$this->load->view('includes/footer');
	}

	function user_cv($id){
		$cv['data'] = $this->welcome_model->view_profile($id);
		
		$this->load->view('includes/header');
		$this->load->view('public/user_cv', $cv);
		$this->load->view('includes/footer');
	}

	private function do_upload($value){
		$type = explode('.', $_FILES[$value]["name"]);
		$type = $type[count($type)-1];
		$url = "./assets/uploads/user_image/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg","jpeg","gif","png","mp4")))
			if(is_uploaded_file($_FILES[$value]["tmp_name"]))
				if(move_uploaded_file($_FILES[$value]["tmp_name"], $url))
					return $url;
		return "";
	}

	function complete_cv(){
		$data = array(
			'user_id' => $this->input->post('txt_hidden'),
			'user_image' =>  $this->do_upload('user_image'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'number' => $this->input->post('number'),
			'preferred_job' => $this->input->post('preferred_job'),
			'father' => $this->input->post('father'),
			'mother' => $this->input->post('mother'),
			'date_of_birth' => $this->input->post('date_of_birth'),
			'gender' => $this->input->post('gender'),
			'marital_status' => $this->input->post('marital_status'),
			'nationality' => $this->input->post('nationality'),
			'religion' => $this->input->post('religion'),
			'permanent_address' => $this->input->post('permanent_address'),
			'current_location' => $this->input->post('current_location'),
			'interested_fields' => $this->input->post('interested_fields'),
			'training_title' => $this->input->post('training_title'),
			'training_topic' => $this->input->post('training_topic'),
			'training_institute' => $this->input->post('training_institute'),
			'training_country' => $this->input->post('training_country'),
			'training_location' => $this->input->post('training_location'),
			'training_year' => $this->input->post('training_year'),
			'training_duration' => $this->input->post('training_duration'),
			'employment_duration' => $this->input->post('employment_duration'),
			'employment_position' => $this->input->post('employment_position'),
			'employment_company' => $this->input->post('employment_company'),
			'employment_address' => $this->input->post('employment_address'),
			'employment_department' => $this->input->post('employment_department'),
			's_major' => $this->input->post('s_major'),
			's_institute' => $this->input->post('s_institute'),
			's_result' => $this->input->post('s_result'),
			's_p_year' => $this->input->post('s_p_year'),
			's_duration' => $this->input->post('s_duration'),
			'c_major' => $this->input->post('c_major'),
			'c_institute' => $this->input->post('c_institute'),
			'c_result' => $this->input->post('c_result'),
			'c_p_year' => $this->input->post('c_p_year'),
			'c_duration' => $this->input->post('c_duration'),
			'u_major' => $this->input->post('u_major'),
			'u_institute' => $this->input->post('u_institute'),
			'u_result' => $this->input->post('u_result'),
			'u_p_year' => $this->input->post('u_p_year'),
			'u_duration' => $this->input->post('u_duration')
		);

		$insert = $this->welcome_model->insert_data($data);
		if($insert){
			$id = $this->input->post('txt_hidden');
			redirect('welcome/cv/'.$id);
		}else{
			$id = $this->input->post('txt_hidden');
			redirect('welcome/user_cv/'.$id);
		}
	}

	function cv($id){
		$getdata['cv'] = $this->welcome_model->get_cv($id);
		
		$this->load->view('includes/header');
		$this->load->view('public/view_cv', $getdata);
	}

	function edit_profile($id){
		$profile['data'] = $this->welcome_model->view_profile($id);
		$profile['sub_category'] = $this->welcome_model->get_post_title();
		$profile['cat_name'] = $this->welcome_model->get_cat_name();
		
		$this->load->view('includes/header');
		$this->load->view('public/edit_profile', $profile);
		$this->load->view('includes/footer');
	}

	function update_profile(){
		$update = $this->welcome_model->update_user_profile();
		if($update){
			$this->session->set_flashdata('submit', 'Profile successfully updated');
		} else{
			$this->session->set_flashdata('not_submit', 'Profile not updated');
		}
		$id = $this->input->post('txt_hidden');
		redirect('welcome/view_profile/'.$id);
	}

	public function post($id){
		$value = $this->welcome_model->fetch_id($id);
		echo "<pre>";
		print_r($value);
		echo "</pre>";
		exit();
	}

	public function skillsharing(){
		$this->load->view('includes/header');
		$this->load->view('public/skillsharing');
		$this->load->view('includes/footer');
	}


	public function explore(){
		$data['category'] = $this->welcome_model->fetch_category();
		$data['posts'] = $this->welcome_model->fetch_all_posts();

		$this->load->view('includes/header');
		$this->load->view('public/explore', $data);
		$this->load->view('includes/footer');
	}

	function category($category){
		$data['sub_category'] = $this->welcome_model->fetch_sub_category($category);
		
		$this->load->view('includes/header');
		$this->load->view('public/category', $data);
		$this->load->view('includes/footer');
	}

	function subCategory($subCategory){

		$data['post'] = $this->welcome_model->fetch_sub_post($subCategory);
		$this->load->view('includes/header');
		$this->load->view('public/sub-category', $data);
		$this->load->view('includes/footer');
	}

	function comment(){
        	$data = array(
        		'c_blog_id' => $this->input->post('c_blog_id'),
        		'c_user_id' => $this->input->post('c_user_id'),
        		'comment' => $this->input->post('comment')
        	);

        	$var = $this->input->post('c_blog_id');

        	$comment['data'] = $this->welcome_model->comment($data);
        	redirect('welcome/posts/'.$var);
        }

	function posts($id){
		$data['post'] = $this->welcome_model->fetch_post($id);
		$data['comment'] = $this->welcome_model->fetch_comment($id);
		$data['rating'] = $this->welcome_model->fetch_rating($id);

		$value = $data['comment'];

        	if($value != null){
        		for ($i = 0; $i < count($value); $i++){
                    $uid = $value[$i]->c_user_id;
                    $name = $this->welcome_model->getUserName($uid);
                    $value[$i]->username = $name[0]['username'];
                    $value[$i]->user_image = $name[0]['user_image'];
                }
        	}
		
		$this->load->view('includes/header');
		$this->load->view('public/posts', $data);
		$this->load->view('includes/footer');
	}

	function updateRating(){
		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');
		$rating = $this->input->post('rating');

		$averageRating = $this->welcome_model->userRating($user_id, $post_id, $rating);

		echo $averageRating;
		exit;
	}

	function add_user_post(){
		$data['sub_category'] = $this->welcome_model->get_sub_cat_name();
		$data['category'] = $this->welcome_model->get_cat_name();
		$this->load->view('includes/header');
		$this->load->view('public/add_post_user', $data);
		$this->load->view('includes/footer');
	}

	 function edit_comment($p_id){

            $var = $this->welcome_model->update_comment();
            if($var){
                redirect('welcome/posts/'.$p_id);
            } else{
                redirect('welcome/posts/'.$p_id);
            }
        }

        function delete_comment($c_id){
            $del = $this->welcome_model->d_comment();
            if($del){
                redirect('welcome/posts/'.$c_id);
            } else{
                redirect('welcome/posts/'.$c_id);
            }
        }

	function query(){
		$this->load->view('includes/header');
		$this->load->view('query/check');
		$this->load->view('includes/footer');
	}

	function query_check(){
		//FETCHING CATEGORY TABLE AS ARRAY OF OBJECTS
		/*Array
		(
		    [0] => Array
		        (
		            [c_id] => 1
		            [c_name] => web
		        )

		    [1] => Array
		        (
		            [c_id] => 2
		            [c_name] => design
		        )

		    [2] => Array
		        (
		            [c_id] => 3
		            [c_name] => desktop
		        )

		)*/
		$data = $this->welcome_model->exam_result();
		//END===================


		//FETCHING DATA FROM FORM AS ARRAY
		/*Array
		(
		    [html] => html
		    [js] => js
		)*/
		 $num = $this->input->post();
		 //END====================

		 

		 //FETCHING SKILL.ID FROM SKILL TABLE USING SKILL.NAME DYNAMICALLY BY FOREACH()
		 //AFTER FETCHING $NUM ARRAY CHANGE AS LIKE BELOW COMMENTED ARRAY:
		 /*Array
		(
		    [html] => 2
		    [js] => 3
		)*/
		 $var = $num;
		 foreach($var as $v){

		 	$this->load->database();
			$this->db->select('s_id');
			$this->db->from('skills');
			$this->db->where('s_name', $v);

			$query = $this->db->get();
			$da = $query->result();

			$num[$v]= $da[0]->s_id;
		 };
		 //END==============



		 //GLOBAL VARIABLE
		 $plus = 0;
		 
		 //FETCHING VALUES FROM VALUE BASED ON CATEGORY ID & SKILL ID;
		 for($i=0; $i<count($data); $i++){ //TO FETCH SKILL VALUES TO ASSIGN IN $data['all 3 indexeds']['skills'] ARRAY
			foreach($num as $v){ //GETTING SKILL VALUE OF ALL THE SKILLS('html', 'js') FOR EVERY SINGLE CATEGORY;
			 	 $this->db->select('value');
				 $this->db->from('value');
				 $this->db->where('c_id', $data[$i]['c_id']);
				 $this->db->where('s_id', $v);
				 $query = $this->db->get();

				 $val = $query->result();

				 $plus += $val[0]->value; //ASSIGNING & SUMMATION OF SKILL VALUES;
				
			 } //END FOREACH;

		 	$data[$i]['skills'] = $plus; //ASSIGNING THE WHOLE PERCENTIGE  VALUE TO THE CATEGORIES('web','desktop','design');
		 	$plus = 0; //re initiating plus as 0 because after the foreach executed it means $plus must be starts from 0 to have the absolute value for the next category.
		 };


		 
		 $values['key'] = $data;
		 $this->load->view('includes/header');
		 $this->load->view('query/data', $values);
		 $this->load->view('includes/footer');
		
	}

	function popular_category(){
		$this->load->view('includes/header');
		$this->load->view('public/popular-category');
		$this->load->view('includes/footer');
	}

	function about_us(){
		$this->load->view('includes/header');
		$this->load->view('public/about');
		$this->load->view('includes/footer');
	}

	function search(){
		$keyword = $this->input->post('search');
		$data['query'] = $this->welcome_model->get_search($keyword);
		$this->load->view('includes/header');
		$this->load->view('search_result', $data);
		$this->load->view('includes/footer');
	}

	function dashboard(){
		if($this->session->userdata('logged')){
			$posts['blogs'] = $this->welcome_model->getPosts();
            $posts['count_posts'] = $this->welcome_model->countPosts();
            $posts['count_users'] = $this->welcome_model->countUsers();
            // $posts['created_by'] = $this->welcome_model->getPosts();

            $data = $posts['blogs'];

            for($i = 0; $i < count($data); $i++){
                $uid = $data[$i]->u_id;
                $name = $this->welcome_model->getUserName2($uid);
                $data[$i]->username = $name[0]->username;
            }

			$this->load->view('includes/header');
			$this->load->view('public/dashboard',$posts);
			$this->load->view('includes/footer');
			} 
			else{
				redirect('welcome');
			}
	}

	function addpost(){
		$this->load->view('includes/header');
		$this->load->view('public/dashboard_add_post');
		$this->load->view('includes/footer');
	}

	function add_post(){
		$add_data = $this->welcome_model->add_post_data();

		if($add_data){
		    redirect('welcome/dashboard');
        } else{
		    redirect('dashboard/addpost');
        }
	}

	function view($id){
        	$data['blog'] = $this->welcome_model->getBlogById($id);
        	$data['comment'] = $this->welcome_model->getComments($id);
        	
        	$value = $data['comment'];

        	if($value != null){
        		for ($i = 0; $i < count($value); $i++){
                    $uid = $value[$i]->c_user_id;
                    $name = $this->welcome_model->getUserName2($uid);
                    $value[$i]->username = $name[0]->username;
                    $value[$i]->user_image = $name[0]->user_image;
                }
        	}

        	// $value[2]->address = "Uttara";
        	// echo "<pre>";
        	// print_r($value);
        	// echo "</pre>";
        	// exit();


        	$this->load->view('includes/header');
        	$this->load->view('public/dashboard_view_post', $data);
        	$this->load->view('includes/footer');
        }

        function editpost($id){
        	$data['blog'] = $this->welcome_model->getBlogById($id);
        	$this->load->view('includes/header');
		    $this->load->view('public/dashboard_edit_post', $data);
		    $this->load->view('includes/footer');
        }

        function deletepost($id){
            $del = $this->welcome_model->d_post($id);
            if($del){
                redirect('welcome/dashboard');
            } else{
                redirect('welcome/dashboard');
            }
        }

        function update(){
        	$result = $this->welcome_model->update();
        	if($result){
        		redirect('welcome/dashboard');
        	} else{
        		redirect('welcome/editpost');
        	}
        }

        function blog_comment(){
        	$data = array(
        		'c_blog_id' => $this->input->post('c_blog_id'),
        		'c_user_id' => $this->input->post('c_user_id'),
        		'comment' => $this->input->post('comment')
        	);

        	$var = $this->input->post('c_blog_id');

        	$comment['data'] = $this->welcome_model->blog_comment($data);
        	redirect('welcome/view/'.$var);
        }

        function edit_comment_blog($p_id){

            $var = $this->welcome_model->blog_update_comment();
            if($var){
                redirect('welcome/view/'.$p_id);
            } else{
                redirect('welcome/view/'.$p_id);
            }
        }

        function delete_comment_blog($c_id){
            $del = $this->welcome_model->blog_d_comment();
            if($del){
                redirect('welcome/view/'.$c_id);
            } else{
                redirect('welcome/view/'.$c_id);
            }
        }

}
