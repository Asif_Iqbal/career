<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('auth_model', 'welcome_model'));
	}

	public function login(){
		if(!$this->session->userdata('logged')){
			$this->load->view('includes/header');
			$this->load->view('auth/login');
			$this->load->view('includes/footer');
		} else{
			redirect('welcome');
		}
		
	}

	private function validate_login(){
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('password', 'trim|required');
		if($this->form_validation->run() == FALSE){
			return FALSE;
		} else{
			return TRUE;
		}
	}

	public function logged(){
		if($this->input->post()){
			if($this->validate_login() === FALSE){
				redirect('auth/login');
			} else{
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$user = $this->auth_model->get($email, $password);
				if($user){
					$this->session->set_userdata('logged', $user[0]->username);
					$this->session->set_userdata('user', $user[0]->id);
					$this->session->set_userdata('u_level', $user[0]->user_level);
					$this->session->set_userdata('interest', $user[0]->interested_field);
					$this->session->set_flashdata('success_login', 'User Successfully Logged In!');
					redirect('welcome/explore');
				} else{
					$this->session->set_flashdata('invalid', 'Invalid User Information');
					redirect('auth/login');
				}
			}
		} else{
			redirect('login/userLogin');
		}
	}

	public function signup(){
		if(!$this->session->userdata('logged')){
			$data['sub_category'] = $this->welcome_model->get_post_title();
			$data['cat_name'] = $this->welcome_model->get_cat_name();
			$this->load->view('includes/header');
			$this->load->view('auth/signup', $data);
			$this->load->view('includes/footer');
		} else{
			redirect('welcome');
		}
	}

	private function validate_registration(){
		$this->form_validation->set_rules('username', 'username', 'trim|required|min_length[2]|max_length[15]');
		$this->form_validation->set_rules('email', 'email', 'trim|required|is_unique[users.email]|valid_email');
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('c_password', 'c_password', 'trim|required|matches[password]');

		if($this->form_validation->run() == TRUE){
			return TRUE;
		} else{
			redirect('auth/signup');
		}
	}

	public function register(){
		if($this->input->post()){
			if($this->validate_registration() === FALSE){
				redirect('auth/signup');
			} else{
				//$message = $this->input->post('message');
				$email = $this->input->post('email');

				$this->load->library('email');

				$this->email->initialize(array(
					'protocol' => 'smtp',
					'smtp_host' => 'smtp.sendgrid.net',
					'smtp_user' => 'Sultanul_Arefin',
					'smtp_pass' => '152154FT39',
					'smtp_port' => 587,
					'crlf' => "\r\n",
					'newline' => "\r\n"
				));

				$username = $this->input->post('username');
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$field = $this->input->post('interested_field');

				$this->email->from('explorecareer@career.com', 'exploreCareer');
				$this->email->to($email);
				$this->email->cc('hello');
				$this->email->bcc('world');
				$this->email->subject('Email Test');
				$this->email->message("Username: ".$username."\n Email: ".$email."\n Password: ".$password."\n Selected field: ".$field);
				$this->email->send();

				$data = [
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'password' => md5($this->input->post('password')),
					'user_level' => "user",
					'user_image' => $this->do_upload('user_image'),
					// 'interested_field' => $this->input->post('interested_field'),
					// 'user_field' => $this->input->post('user_field')
				];

				$user = $this->auth_model->insert($data);
				if($user){
					$this->session->set_flashdata('success', 'User successfully Registered');
					redirect('auth/login');
				} else{
					redirect('auth/signup');
				}
			}
		}
	}

	private function do_upload($value){
		$type = explode('.', $_FILES[$value]["name"]);
		$type = $type[count($type)-1];
		$url = "./assets/uploads/user_image/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg","jpeg","gif","png","mp4")))
			if(is_uploaded_file($_FILES[$value]["tmp_name"]))
				if(move_uploaded_file($_FILES[$value]["tmp_name"], $url))
					return $url;
		return "";
	}

	public function logout(){
		$this->session->unset_userdata('logged');
			$this->session->sess_destroy();
			redirect('welcome');
		}

	

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */