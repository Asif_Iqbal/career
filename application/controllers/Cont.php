<?php

	class Cont extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('users');
		}

		public function user()
		{
		    $post_id = 1; //this is static id of blog but you are not enter static id you enter your dynami c id of blog or post
		    $data['get_avg_rating'] = $this->users->count_total_rating($post_id);
		    $data['rating_data'] = $this->users->get_rating_data($post_id);
		    $this->load->view('user', $data);
		}
	}

	?>