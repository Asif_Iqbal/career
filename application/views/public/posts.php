
<div class="container container_explore">

    <h2 class="primary_heading_explore">
        Details for <span><?= $post[0]->post_heading; ?></span>
    </h2>
   
    <?php if($post != null): ?>
        <?php foreach($post as $value): ?>
    <!-- <div class="row explore_card relative_pos">
        <p class="category">
            Discover Career Of <span><?= $value->sub_cat_name; ?></span>
        </p> -->
        <!-- <div class="col-md-6 border-right">
            <div class="video_container">
                <video src="<?= base_url().$value->video_url; ?>" controls></video>
            </div>
        </div> -->
        <!-- <div class="col-md-6 vertical_middle">
            <div class="description_container">
                <h3 class="desc_heading">
                    <?= $value->post_heading; ?>
                </h3>

                <p class="short_desc">
                <?= $value->post_description; ?>
                </p>
            </div>
        </div> -->

        <div class="col-12 post_method ">
        <div>
        <h2 class="primary_heading_explore post_requirement">
         <span>Education Requirement:</span>
        </h2>
        </div>
      
        <span>
        <p class="post_details">
            <?= $value->requirments; ?>
        </p>
        </span>

        <div>
        <h2 class="primary_heading_explore post_skills">
         <span>Institution:</span>
        </h2>
        </div>
        <p class="post_details para"><span class="skills_short"><?= $value->institution; ?></span></p>
       

        <div>
        <h2 class="primary_heading_explore post_skills">
         <span>SKILLS:</span>
        </h2>
        </div>
        <p class="post_details para"><span class="skills_short"><?= $value->skills; ?></span></p>

        

        <div>
        <h2 class="primary_heading_explore post_skills">
         <span>Demands:</span>
        </h2>
        </div>
        <div class="demands">
        <p style="padding-left: 30px;"><?= $value->demand; ?></p>
        </div>
        <div>
        <h2 class="primary_heading_explore post_skills">
         <span>Conclusion:</span>
        </h2>
        </div>
        <p class="post_details para conlution"><?= $value->conclusion; ?></p>
        </span>
        </div>
   
    </div>
<?php endforeach; ?>
<?php endif; ?>


<div>
    <?php foreach($rating as $rate): ?>

    <input type="hidden" name="user_id" value="<?= $this->session->userdata('user'); ?>">
    <input type="number" id="star-rating-demo" value="<?= $rate->rating; ?>" class="rating post_<?= $rate->post_id ?>" min=0 max=5 step=0.1 data-size="sm">

    <div>Average Rating: <?= $rate->rating; ?> <span id="averagerating"></span></div>
    <?php endforeach; ?>
</div>

<!-- Script -->
    <script type='text/javascript'>
    
    // Rating change
      $('.rating').on('change', function(event, value, caption) {
        var id = this.className.split(/\s+/);
        var splitid = id[1].split('_');
        var post_id = splitid[1];
        var value =  $('input#star-rating-demo').val();

        console.log(value);
        $.ajax({
          url: '<?= base_url() ?>welcome/updateRating',
          type: 'post',
          data: {post_id: post_id, rating: value},
          success: function(response){
             $('#averagerating_'+post_id).text(response);
          }
        });
      });
    </script>



<section id="posts">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="card-header">
                    <div>
                        <div class="row">
                            <div class="span8">                                
                                <div>
                                    <h3>Opinion:</h3>
                                    <br>
                                    <?php if ($comment != null) : ?>
                                    <div class="comment_container">
                                    <?php foreach ($comment as $value) : ?>
                                        <div class="comment_container_inner clearfix">
                                            <div class="user_img_container">

                                                <?php if($value->user_image != null){ ?>
                                                    <img src="<?= base_url($value->user_image); ?>" alt="photo of <?php echo $value->username ?>" class="user_photo" title="<?php echo $value->username ?>">
                                                    
                                                <?php }else{ ?>

                                                <img src="<?= base_url('assets/uploads/default_profile.png'); ?>" alt="photo of <?php echo $value->username ?>" class="user_photo" title="<?php echo $value->username ?>">
                                                <?php } ?>
                                                
                                            </div>
                                            <div class="coment_area">
                                                <span class="user_name">
                                                    <?php echo $value->username ?>
                                                </span>

                                                <span class="user_comment">
                                                    <?php echo $value->comment; ?>
                                                </span>
                                            <?php if ($this->session->userdata('user') == $value->c_user_id) { ?>
                                                <form action="<?= base_url('welcome/edit_comment/') . $value->c_blog_id; ?>" method="post" id="update_form">
                                                    <input type="hidden" name="c_id" value="<?php echo $value->id; ?>">

                                                    <textarea name="update_comment" id="update_comment" class="update_comment" value="" rows="auto"></textarea>

                                                    <button class="btn btn-default btn-small pull-right" type="reset" onclick="hideUpdateForm(this)">Cancel</button>
                                                    <input type="submit" value="save" class="btn btn-default btn-small pull-right" style="margin-right:5px">
                                                </form>
                                                <?php 
                                            } ?>   
                                            </div>

                                            <?php if ($this->session->userdata('user') == $value->c_user_id) { ?>
                                                <div class="edit_delete">
                                                    <a href="#" class="toPrevent" onclick="showUpdateForm(this)"><i class="fa fa-edit">
                                                    </i></a> 
                                                    <form action="<?= base_url('welcome/delete_comment/') . $value->c_blog_id; ?>" method="post" class="delete_form">
                                                        <input type="hidden" name="post_id" value="<?= $value->id; ?>">
                                                        <button type="submit" class="toPrevent1" onclick="return confirm('Do you want to delete this record?');"><i class="fa fa-trash">
                                                    </i></button>
                                                    </form>   
                                                </div>
                                            <?php 
                                        } ?>        
                                        </div>
                                    <?php endforeach; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php if(!$this->session->userdata('logged')): ?>
                                    <div>
                                        <p>Please <a href="<?= base_url('auth/login'); ?>">Login </a>to comment</p>
                                    </div>
                                <?php endif; ?>
                                <div>
                                    <?php if($this->session->userdata('logged')): ?>
                                        <form method="post" action="<?php echo base_url('welcome/comment'); ?>">
                                            <input type="hidden" name="c_blog_id" value="<?php echo $post[0]->id; ?>">
                                            <input type="hidden" name="c_user_id" value="<?php echo $this->session->userdata('user'); ?>">
                                            <textarea rows="4" cols="40" name="comment" placeholder="Comment here..."></textarea>
                                            <button type="submit" style="color: black;" class="btn btn-default">Comment</button>
                                        </form>
                                <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <hr>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= base_url('assets/js/comment.js')?>"></script>