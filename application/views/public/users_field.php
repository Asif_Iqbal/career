<br><br>
	<link rel="stylesheet" href="<?= base_url('assets/css/tips.css') ?>">

    <h2 class="primary_heading_explore">
        Because You've Chosen: <span><?= $user->user_field; ?></span>
    </h2>
        
            <div class="row">
                <?php foreach($data as $users_field): ?>
                <div class="col-sm-3 basic-img3 asif1 asif2">
                    <div class="basic-img1">
                        <video src="<?= base_url().$users_field->video_url; ?>" controls></video>
                    </div>
                    <div class="para1">
                        <div class="basics-title"><?php echo $users_field->sub_cat_name; ?></div>
                        <div class="basics-para">
                            <p>Use Job Agent To speed Up Your Job Search</p>
                            <a href="<?=base_url('') ?>welcome/posts/<?= $users_field->id; ?>" class="btn btn-info btn-lg" title="Read Details about <?= $users_field->post_title; ?>">View Details</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
