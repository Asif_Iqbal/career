<div class="container container_explore">

    <?php if($this->session->flashdata('success_login')): ?>
        <div class="login">
          <?php echo $this->session->flashdata('success_login'); ?>
        </div>
    <?php endif; ?>

    <?php if($category != null){ ?>
        <h2 class="primary_heading_explore">
            DISCOVER CAREER PATH's AND CHOOSE YOUR's
        </h2>
        <?php foreach($category as $data): ?>
    <div class="row explore_card relative_pos explore_card1">
        <p class="category">
            Discover Career's Of <span><?php echo $data->cat_name; ?></span> Background
        </p>
        <div class="col-md-6 border-right">
            <div class="video_container">
                <video src="<?= base_url().$data->video_url; ?>" controls></video>
            </div>
        </div>
        <div class="col-md-6 vertical_middle">
            <div class="description_container">
                <h3 class="desc_heading">
                   <?php echo $data->cat_heading; ?>
                </h3>

                <p class="short_desc">
                    <?php echo $data->cat_description; ?>
                </p>

                <center>
                    <a href="<?= base_url() ?>welcome/category/<?php echo $data->cat_name; ?>" class="btn btn-info btn-lg btn11" title="Explore All Career Path's From <?php echo $data->cat_name; ?> Background">View All</a>
                </center>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php }else{ ?>
    <h2 class="primary_heading_explore">
        NO DATA AVAILABLE
    </h2>
<?php } ?>
</div>


<link rel="stylesheet" href="<?= base_url('assets/css/tips.css') ?>">
        
            <div class="row">
                <?php foreach($posts as $post): ?>
                <div class="col-sm-3 basic-img3 asif1">
                    <div class="basic-img1">
                        <video src="<?= base_url().$post->video_url; ?>" controls></video>
                    </div>
                    <div class="para1">
                        <div class="basics-title"><?php echo $post->post_title; ?></div>
                        <div class="basics-para">
                            <p>Background Needed: <span style="color: red;"><b><?= $post->cat_name ?></b></span></p>
                            <a href="<?=base_url('') ?>welcome/posts/<?= $post->id; ?>" class="btn btn-info btn-inzo btn-lg" title="Read Details about <?= $post->post_title; ?>">View Details</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        
