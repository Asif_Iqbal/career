<div class="home_background1">
 <div class="container home_content_container">
        <div class="content_container_inner">
            <h3 class="home_heading home_heading1 wow fadeInUp" data-wow-delay='.2s'>
                GIVE IDEA TO SHARE WITH OTHER USERS
            </h3>

            <div style="text-align: center;">
                <form method="post" action="<?= base_url('admin/save_user_post') ?>" enctype="multipart/form-data">
                <select name="cat_name" class="custom-select select form-group wow fadeInUp" data-wow-delay='.2s'>
                    <option selected> Select Category</option>
                    <?php foreach($category as $data): ?>
                    <option value="<?= $data->cat_name; ?>"><?= $data->cat_name; ?></option>
                    <?php endforeach; ?>
                </select> 

                <select name="sub_cat_name" class="custom-select select form-group wow fadeInUp" data-wow-delay='.3s'>
                    <option selected> Select Sub Category</option>
                    <?php foreach($sub_category as $data): ?>
                    <option value="<?= $data->sub_cat_name; ?>"><?= $data->sub_cat_name; ?></option>
                    <?php endforeach; ?>
                </select> 
      
      <!-- form-group// -->
    <!-- form-group// -->

                    <div class="form-group input-group group_mg_btm input-group input-group1 wow fadeInUp" data-wow-delay='.2s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input class="form-control fch" placeholder="post title" type="text" name="post_title">
                    </div> <!-- form-group// -->
                    <div class="form-group input-group group_mg_btm input-group1 wow fadeInUp" data-wow-delay='.2s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input class="form-control fch" placeholder="post heading" type="text" name="post_heading">
                    </div> 

                    <div class="form-group input-group group_mg_btm input-group1 wow fadeInUp" data-wow-delay='.3s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <textarea class="form-control" placeholder="post description" rows="5" name="post_description"></textarea>
                    </div> 

                    <div class="form-group input-group group_mg_btm input-group1 wow fadeInUp" data-wow-delay='.3s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-file-video-o"></i> </span>
                        </div>
                        <input type="file" id="myFile" name="video_url">
                    </div>
                    
                    <div class="form-group input-group group_mg_btm input-group1 wow fadeInUp" data-wow-delay='.4s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-bell-o"></i> </span>
                        </div>
                        <input class="form-control fch" placeholder="requirments" type="text" name="requirments">
                    </div>

                    <div class="form-group input-group group_mg_btm input-group1 wow fadeInUp" data-wow-delay='.5s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-institution"></i> </span>
                        </div>
                        <input class="form-control fch" placeholder="institution" type="text" name="institution">
                    </div>

                    <div class="form-group input-group group_mg_btm input-group1 wow fadeInUp" data-wow-delay='.6s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-flag"></i> </span>
                        </div>
                        <input class="form-control fch" placeholder="demand" type="text" name="demand">
                    </div>

                    <div class="form-group input-group group_mg_btm input-group1 wow fadeInUp" data-wow-delay='.6s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-group"></i> </span>
                        </div>
                        <input class="form-control fch" placeholder="skills" type="text" name="skills">
                    </div>

                    <div class="form-group input-group group_mg_btm input-group1
                    wow fadeInUp" data-wow-delay='.5s'>
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-tasks"></i> </span>
                        </div>
                        <input class="form-control fch" placeholder="conclusion" type="text" name="conclusion">
                    </div>
                    <input placeholder="conclusion" type="hidden" name="status" value="pending">
                    
                    
                    
                    
                
                    <!-- form-group// -->
                    
                    <!-- form-group// -->

                    <div class="form-group wow fadeInUp" data-wow-delay='.3s'">
                        <button type="submit" class="btn btn-primary btn-block">Insert</button>
                    </div> <!-- form-group// -->    

                                                                                     
                </form>
            </div>
         
        </div>
    </div>
</div>