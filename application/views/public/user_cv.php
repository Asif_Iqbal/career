<br><br>
<div class="container container_explore">
	<form method="post" action="<?php echo base_url('welcome/complete_cv'); ?>" enctype="multipart/form-data">
		<table>
			<h1>Personal Details:</h1>
			<input type="hidden" name="txt_hidden" value="<?php echo $data->id; ?>">
			<tr>
				<td>Update Photo: </td>
				<td><input type="file" name="user_image" class="form-control fch"></td>
			</tr>
			<tr>
				<td>Username: </td>
				<td><input type="text" value="<?php echo $data->username; ?>" name="username"></td>
			</tr>
			<tr>
				<td>Email: </td>
				<td><input type="email" value="<?php echo $data->email; ?>" name="email"></td>
			</tr>
			<tr>
				<td>Contact Number: </td>
				<td><input type="text" name="number"></td>
			</tr>
			<tr>
				<td>Preferred Job Category: </td>
				<td><input type="text" name="preferred_job">
				</td>
			</tr>
			<tr>
				<td>Father's Name: </td>
				<td><input type="text" name="father">
				</td>
			</tr>
			<tr>
				<td>Mother's Name: </td>
				<td><input type="text" name="mother">
				</td>
			</tr>
			<tr>
				<td>Date of Birth: </td>
				<td><input type="text" name="date_of_birth">
				</td>
			</tr>
			<tr>
				<td>Gender: </td>
				<td><input type="text" name="gender">
				</td>
			</tr>
			<tr>
				<td>Marital Status: </td>
				<td><input type="text" name="marital_status">
				</td>
			</tr>
			<tr>
				<td>Nationality: </td>
				<td><input type="text" name="nationality">
				</td>
			</tr>
			<tr>
				<td>Religion: </td>
				<td><input type="text" name="religion">
				</td>
			</tr>
			<tr>
				<td>Permanent Address: </td>
				<td><input type="text" name="permanent_address">
				</td>
			</tr>
			<tr>
				<td>Current Location: </td>
				<td><input type="text" name="current_location">
				</td>
			</tr>
			<tr>
				<td>Interested Fields: </td>
				<td><input type="text" name="interested_fields">
				</td>
			</tr>
			<tr><td><h1>Training Summary: </h1><hr></td></tr>
			<tr>
				<td>Training Title: </td>
				<td><input type="text" name="training_title">
			</tr>
			<tr>
				<td>Topic: </td>
				<td><input type="text" name="training_topic">
			</tr>
			<tr>
				<td>Institute: </td>
				<td><input type="text" name="training_institute">
			</tr>
			<tr>
				<td>Country: </td>
				<td><input type="text" name="training_country">
			</tr>
			<tr>
				<td>Location: </td>
				<td><input type="text" name="training_location">
			</tr>
			<tr>
				<td>Year: </td>
				<td><input type="text" name="training_year">
			</tr>
			<tr>
				<td>Duration: </td>
				<td><input type="text" name="training_duration">
			</tr>
			<tr>
				<td><h1>Employment History</h1><hr></td>
			</tr>
			<tr>
				<td>Duration: </td>
				<td><input type="text" name="employment_duration">
			</tr>
			<tr>
				<td>Positon: </td>
				<td><input type="text" name="employment_position">
			</tr>
			<tr>
				<td>Company: </td>
				<td><input type="text" name="employment_company">
			</tr>
			<tr>
				<td>Address: </td>
				<td><input type="text" name="employment_address">
			</tr>
			<tr>
				<td>Department: </td>
				<td><input type="text" name="employment_department">
			</tr>
			<tr>
				<td><h1>Academic Qualification</h1><hr></td>
			</tr>
			<tr>
				<td><h2>School</h2><hr></td>
			</tr>
			<tr>
				<td>Major: </td>
				<td><input type="text" name="s_major">
			</tr>
			<tr>
				<td>Institute: </td>
				<td><input type="text" name="s_institute">
			</tr>
			<tr>
				<td>Result: </td>
				<td><input type="text" name="s_result">
			</tr>
			<tr>
				<td>Passing Year: </td>
				<td><input type="text" name="s_p_year">
			</tr>
			<tr>
				<td>Duration: </td>
				<td><input type="text" name="s_duration">
			</tr>
			<tr><td><h2>College</h2><hr></td></tr>
			<tr>
				<td>Major: </td>
				<td><input type="text" name="c_major">
			</tr>
			<tr>
				<td>Institute: </td>
				<td><input type="text" name="c_institute">
			</tr>
			<tr>
				<td>Result: </td>
				<td><input type="text" name="c_result">
			</tr>
			<tr>
				<td>Passing Year: </td>
				<td><input type="text" name="c_p_year">
			</tr>
			<tr>
				<td>Duration: </td>
				<td><input type="text" name="c_duration">
			</tr>
			<tr><td><h2>University</h2><hr></td></tr>
			<tr>
				<td>Major: </td>
				<td><input type="text" name="u_major">
			</tr>
			<tr>
				<td>Institute: </td>
				<td><input type="text" name="u_institute">
			</tr>
			<tr>
				<td>Result: </td>
				<td><input type="text" name="u_result">
			</tr>
			<tr>
				<td>Passing Year: </td>
				<td><input type="text" name="u_p_year">
			</tr>
			<tr>
				<td>Duration: </td>
				<td><input type="text" name="u_duration">
			</tr>
			<tr>
				<td></td>
				<td><button type="submit" class="btn btn-info">Create CV</button></td>
			</tr>
		</table>
	</form>
	<br><br>
</div>