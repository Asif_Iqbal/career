<br><br><div class="container container_explore container_explore1">
	<?php if($this->session->flashdata('submit')): ?>
		<?php echo $this->session->flashdata('submit'); ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('not_submit')): ?>
		<?php echo $this->session->flashdata('not_submit'); ?>
	<?php endif; ?>
	<table class="profile">
		<tr>
			<td>Username: </td>
			<td><?php echo $data->username; ?></td>
		</tr>
		<tr>
			<td>Email: </td>
			<td><?php echo $data->email; ?></td>
		</tr>
		<tr>
			<td>User Level: </td>
			<td><?php echo $data->user_level; ?></td>
		</tr>
		<tr>
			<td>Created On: </td>
			<td><?php echo $data->created_on; ?></td>
		</tr>
		<tr>
			<td>Interested Field: </td>
			<td><?php echo $data->interested_field; ?></td>
		</tr>
		<tr>
			<td>Your Field: </td>
			<td><?php echo $data->user_field; ?></td>
		</tr>
	</table>
	
	<img src="<?php echo base_url($data->user_image); ?>" alt="User Image" height="60px" width="90px"><br><br>
	<a class="btn btn-info" href="<?php echo base_url('welcome/edit_profile/').$data->id; ?>">Edit Profile</a>
</div>