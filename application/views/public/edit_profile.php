<br><br><div class="container container_explore container_explore1">
	<img src="<?php echo base_url($data->user_image); ?>" alt="User Image" height="60px" width="90px">
	<form method="post" action="<?php echo base_url('welcome/update_profile'); ?>" enctype="multipart/form-data">
		<table class="profile">
			<input type="hidden" name="txt_hidden" value="<?php echo $data->id; ?>">
			<tr>
				<td>Username: </td>
				<td><input type="text" value="<?php echo $data->username; ?>" name="username"></td>
			</tr>
			<tr>
				<td>Email: </td>
				<td><input type="email" value="<?php echo $data->email; ?>" name="email"></td>
			</tr>
			<tr>
				<td>Interested Field: </td>
				<td>
					<select name="interested_field" class="custom-select select form-group edit_cv">
	                <option selected><?php echo $data->interested_field; ?></option>
	                <?php foreach($sub_category as $data): ?>
	                <option value="<?= $data->post_title; ?>"><?= $data->post_title; ?></option>
	                <?php endforeach; ?>
           		 </select> 
				</td>
			</tr>
			<tr>
				<td>Your Field: </td>
				<td>
					<select name="user_field" class="custom-select select form-group edit_cv">
	                <option selected> Select Your Field</option>
	                <?php foreach($cat_name as $data): ?>
	                <option value="<?= $data->cat_name; ?>"><?= $data->cat_name; ?></option>
	                <?php endforeach; ?>
           		 </select> 
				</td>
			</tr>
			
			<tr>
				<td>User Photo: </td>
				<td><input type="file" name="user_image" class="form-control fch"></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit" class="btn btn-info">Update Profile</button></td>
			</tr>
		</table>
	</form>
	<br><br>
</div>