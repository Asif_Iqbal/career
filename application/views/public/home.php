      <link rel="stylesheet" href="<?= base_url('assets/css/card.css') ?>">
            
            <!-- header content starts -->
    <div class="header container-fluid home_background">
            
        <div class="header_content">
           
            
            <p class="welcome_msg wow fadeInUp"data-wow-delay=".1s">
                Hello!! Welcome To 
            </p>
            <h3 class="primary_heading wow fadeInUp" data-wow-delay=".3s"data-text='Explore Career'>
                Explore.Career..
            </h3>

            <!-- header search -->
            <!-- <form  method="post" action="<?= base_url('welcome/search'); ?>">
                <div class="input-group mb-3 wow fadeInDown" data-wow-delay=".10s">
                    <input type="text" name="search" class="form-control search_input" placeholder="Search Query" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <input class="btn btn-success search_btn" type="submit" value="Submit">
                    </div>
                </div>
            </form> -->
            
            <!-- <button class="btn btn-light"><?php echo $this->session->userdata('interest'); ?></button> -->
        </div>

    </div>
    <!-- header content ends -->

    <!-- Home Content Starts-->

    <div class="ticker-container">
        <div class="ticker-caption">
            <p>Job News</p>
        </div>
        <ul>
            <?php foreach($sticker as $stick): ?>
            <div>
                <li><span>Job Portal <?php echo $stick->id; ?> &ndash; <a href="<?php echo $stick->url; ?>"><?php echo $stick->title; ?></a></span></li>
            </div>
        <?php endforeach; ?>
            
        </ul>
    </div>


       <!-- <div class="container home_content_container">
            <h1 class="home_heading">Blog</h1><hr>
            <?php foreach($posts as $row){ ?>


                <!-- <h1 class="home_desc"><a href="<?= base_url('welcome/posts/').$row->id; ?>"><?= $row->post_title; ?></a></h1>
                <p><?= "Sub_Category_Name: ".$row->sub_cat_name ?></p>
                <p><?= "Category_Name: ".$row->cat_name ?></p>
                <p><?= "Post_Heading: ".$row->post_heading ?></p>
                <p><?= "Post_Description: ".$row->post_description ?></p>
                <p><a href="<?= base_url('welcome/posts/').$row->id; ?>">Read More</a></p><hr> -->

              <!--  <div class="qoutes">
                    <h4 class="qoute_heading">
                        <?= "Post_Heading: ".$row->post_heading ?>
                    </h4>
                    <p class="qoute_desc">
                        <?= "Post_Description: ".$row->post_description ?>
                    </p>
                    <a class="btn btn-danger" href="<?= base_url('welcome/posts/').$row->id; ?>">
                        Read More
                    </a>
                    <p class="home_desc">
                        <?= $pages; ?>
                    </p>
                </div>

            <?php } ?>
        
        </div> -->



    <div class="container home_content_container">
        <div class="content_container_inner">
            <h3 class="home_heading fadeInUp wow" data-wow-delay=".2s">
                ADVICE FOR MAKING A FRESH START
            </h3>

            <p class="home_desc fadeInDown wow" data-wow-delay=".3s">
                Life is full of new beginnings. Here’s some valuable advice to help you along the way.
            </p>

            <div class="qoutes fadeInUp wow" data-wow-delay=".2s">
                <h4 class="qoute_heading">
                    STEVE JOBS – STANFORD UNIVERSITY, 2005
                </h4>
                <p class="qoute_desc">
                    "You've got to find what you love. And that is as true for your work as it is for your lovers. Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do."
                </p>
            </div>

            <div class="qoutes fadeInUp wow" data-wow-delay=".4s">
                <h4 class="qoute_heading">
                    STEPHEN COLBERT – NORTHWESTERN UNIVERSITY, 2011
                </h4>
                <p class="qoute_desc">
                
                    "If we'd all stuck with our first dream, the world would be overrun with cowboys and princesses. So whatever your dream is right now, if you don't achieve it, you haven't failed, and you're not some loser. But just as importantly — and this is the part I may not get right and you may not listen to — if you do get your dream, you are not a winner."
                </p>
            </div>


            <div class="qoutes fadeInUp wow" data-wow-delay=".6s">
                <h4 class="qoute_heading">
                    NORA EPHRON – WELLESLEY COLLEGE, 1996
                </h4>
                <p class="qoute_desc">
                    "What are you going to do? Everything, is my guess. It will be a little messy, but embrace the mess. It will be complicated, but rejoice in the complications. It will not be anything like what you think it will be like, but surprises are good for you. And don't be frightened: You can always change your mind. I know: I've had four careers and three husbands."
                </p>
            </div>
        </div>
    </div>
    <!-- Home Content ends-->

   





<!-- Card Start-->


<div class="contact-header">
    <h2 class="contact-title wow shake">Which is best for you?</h2>
</div>
<div class="row">
    <div class="col-1-of-3 wow fadeInDown" data-wow-delay=".2s">
        <div class="card">
            <div class="cardf">
                <div class="card-picture">
                    
                </div>
                <h4 class="card-heading card-sci">
                    <span class="card-heading-span ">BASICS</span>
                </h4>
                <div class="card-details">

                    <p>How Skillsharing Can Help Your Career</p>

                    <ul>
                        <li>What Is Skillsharing?</li>
                        <li>The Benefits of Skillsharing</li>
                        <li>Skillsharing Online
                        </li>
                        <li>Community Skillsharing</li>
                    </ul>
                </div>

            </div>
            <div class="cardb cardb__color1">
                <div class="card-cta">
                    <div class="card-price-box">
                        <p class="card-price-only">Your Choice</p>
                        <p class="card-price-value">Skillsharing</p>
                    </div>
                    <a href="<?php echo base_url('welcome/skillsharing'); ?>" class="btn1 btn-click ">Click Here</a>
                </div>
            </div>


        </div>
    </div>


    <div class="col-1-of-3 wow fadeInUp" data-wow-delay=".4s">
        <div class="card fadeInUp wow" data-wow-delay=".2s">
            <div class="cardf">
                <div class="card-picture1">
                   
                </div>
                <h4 class="card-heading card-com">
                    <span class="card-heading-span ">BASICS</span>
                </h4>
                <div class="card-details">
                    <p>7 Best Jobs for Single Moms Starting Over</p>
                    <ul>
                        <li>Teacher</li>
                        <li> Healthcare Professional</li>
                        <li>Sales</li>
                        <li> Marketing Professional</li>
                       


                    </ul>
                </div>

            </div>
            <div class="cardb cardb__color2">
                <div class="card-cta">
                    <div class="card-price-box">
                        <p class="card-price-only">Your Are</p>
                        <p class="card-price-value">Single Moms</p>
                    </div>
                    <a href="#" class="btn1 btn-click">Click Here</a>
                </div>
            </div>

        </div>
    </div>



    <div class="col-1-of-3 wow fadeInDown" data-wow-delay=".6s">
        <div class="card">
            <div class="cardf">
                <div class="card-picture2">
                    &nbsp;
                </div>
                <h4 class="card-heading card-arts">
                    <span class="card-heading-span ">BASICS</span>
                </h4>
                <div class="card-details">
                    <p>Top 15 Kids' Dream Jobs</p>
                    <ul>
                        <li> Dancer/Choreographer</li>
                        <li> Actor</li>
                        <li>Musician</li>
                        <li> Teacher</li>
                        

                    </ul>
                </div>

            </div>
            <div class="cardb cardb__color3">
                <div class="card-cta">
                    <div class="card-price-box">
                        <p class="card-price-only">Your Are</p>
                        <p class="card-price-value">Kids</p>
                    </div>
                    <a href="#" class="btn1 btn-click">Click Here</a>
                </div>
            </div>

        </div>
    </div>
</div>


    <a href="tips.html" class="btn1 btn-discover fadeInUp wow" data-wow-delay=".2s">Discover All Career</a>
</div>

<br><br>


<!-- Card End-->
 <script>
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            // Do something
            if(scroll > 0){
                $('.explore_nav').addClass('explore_nav_common');
            }else{
                $('.explore_nav').removeClass('explore_nav_common');
            }
        });
    </script>
    