<br><br>
<title><?= $cv->username; ?></title>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/cv.css'); ?>">
<?php foreach($cv as $data): ?>
<div class="row cv">
        <div class="col-sm-9">
            <div class="cv_name">
                <h2><?= $data->username; ?></h2>
            </div>
            <div class="cv_contact family">
                <p><?= $data->permanent_address; ?></p>
                <p><?= $data->number; ?>
                </p>
                <p>e-mail : <?= $data->email; ?>
                </p>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="cv_img">
                <img src="<?= base_url($data->user_image); ?>" alt="cv-img" />
            </div>
        </div>

    </div>
    <div class="cv_detailes">
        <div class="cv_history">
            <h3>Employment History:
            </h3>
        </div>
        <div>
            <p> <b>Total Year of Experience :</b> <?= $data->employment_duration; ?>(s)
            </p>
        </div>
        <div class="cv_intern">
            <h4><b>Duration: <?= $data->employment_duration; ?>
            </b></h4>
        </div>
        <div class="cv_company">
            <p><b><?= $data->employment_company; ?>
                </b></p>
            <p><?= $data->employment_address; ?>
            </p>
            <p>Department: <?= $data->employment_department; ?>
            </p>
        </div>

        <div class="cv_history">
            <h3>Academic Qualification:

            </h3>
        </div>
    </div>
    <div class="cv_exam">
        <table>
            <tr>
                <th>Exam Title</th>
                <th>Concentration/Major</th>
                <th>Institute</th>
                <th>Result</th>
                <th>Pas.Year</th>
                <th>Duration</th>
            </tr>
            <tr>
                <td>School</td>
                <td><?= $data->s_major; ?></td>
                <td><?= $data->s_institute; ?>
                </td>
                <td>CGPA:<?= $data->s_result; ?> </td>
                <td><?= $data->s_p_year; ?></td>
                <td><?= $data->s_duration; ?></td>
            </tr>
            <tr>
                <td>College</td>
                <td><?= $data->c_major; ?></td>
                <td><?= $data->c_institute; ?></td>
                <td>CGPA:<?= $data->c_result; ?> </td>
                <td><?= $data->c_p_year; ?></td>
                <td><?= $data->c_duration; ?></td>
            </tr>
            <tr>
                <td>University</td>
                <td><?= $data->u_major; ?></td>
                <td><?= $data->u_institute; ?></td>
                <td>CGPA:<?= $data->u_result; ?> </td>
                <td><?= $data->u_p_year; ?></td>
                <td><?= $data->u_duration; ?></td>
            </tr>
        </table>
    </div>


    <div class="cv_history training">
        <h3>Training Summary:
        </h3>
    </div>
    <div class="cv_exam">
        <table>
            <tr>
                <th>Training Title</th>
                <th>Topic</th>
                <th>Institute</th>
                <th>Country</th>
                <th>Location</th>
                <th>Year</th>
                <th>Duration</th>
            </tr>
            <tr>
                <td><?= $data->training_title; ?></td>
                <td><?= $data->training_topic; ?></td>
                <td><?= $data->training_institute; ?></td>
                <td><?= $data->training_country; ?></td>
                <td><?= $data->training_location; ?>
                </td>
                <td><?= $data->training_year; ?></td>
                <td><?= $data->training_duration; ?></td>

            </tr>
        </table>

        <div class="cv_history qualification">
            <h3>Professional Qualification:

            </h3>
        </div>
        <table>
            <tr>

                <th>Certification</th>
                <th>Institute</th>
                <th>Location</th>
                <th>Department</th>
                <th>Duration</th>

            </tr>
            <tr>
                <td><?= $data->employment_position; ?></td>
                <td><?= $data->employment_company; ?></td>
                <td><?= $data->employment_address; ?></td>
                <td><?= $data->employment_department; ?></td>
                <td><?= $data->employment_duration; ?></td>

            </tr>
        </table>

        <div class="cv_history qualification">
            <h3>Career and Application Information:

            </h3>

        </div>
        <div class="family">
            <p>Preferred Job Category :<span class="it"> <?= $data->preferred_job; ?></span>
            </p>
        </div>
        <div class="cv_history qualification">
            <h3>Personal Details :

            </h3>

        </div>
        <div class="family">
            <p>Father's Name <span class="it1">: <?= $data->father; ?></span></p>
            <p>Mother's Name <span class="it2">: <?= $data->mother; ?></span></p>
            <p>Date of Birth <span class="it3">: <?= $data->date_of_birth; ?></span></p>
            <p> Gender <span class="it4">: <?= $data->gender; ?></span></p>
            <p>Marital Status <span class="it5">: <?= $data->marital_status; ?></span></p>
            <p> Nationality<span class="it6">: <?= $data->nationality; ?></span></p>
            <p>Religion <span class="it7">: <?= $data->religion; ?></span></p>
            <p>Permanent Address <span class="it8">: <?= $data->permanent_address; ?></span></p>
            <p> Current Location<span class="it9">: <?= $data->current_location; ?></span></p>
        </div>
        <?php endforeach; ?>
        <br><br>