
<section id="posts">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="card-header">
                    <h4>Latest Posts</h4>

                    <div>
                        
                        <div class="row">
                            <div class="span8">
                                <div class="row">
                                    <div class="span8">
                                        <h2><strong><a href="#"><?php echo $blog->title; ?></a></strong></h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="span2">
                                        <a href="#" class="thumbnail">
                                            <img src="http://www.beshto.com/beshtoImages/photoShare/d535/u38841/u38841_700055_575149.jpg" alt="kodom ful">
                                        </a>
                                    </div>
                                    <div class="span6">
                                        <p style="color: red; font-size:2.8rem;">
                                            <?php echo $blog->body; ?>
                                        </p>
                                        <?php if ($this->session->userdata('user') == $blog->u_id) { ?>
                                            <a class="btn btn-danger" style="color: black;" href="<?php echo base_url('welcome/editpost/') . $blog->id; ?>">Edit</a>
                                            <!-- <button class="btn btn-default"><a style="color: black;" href="<?php echo base_url('dashboard/view/') . $blog->id; ?>">Comment</a></button> -->
                                            <a class="btn btn-dark" style="color: black;" href="<?php echo base_url('welcome/deletepost/') . $blog->id; ?>" onclick="return confirm('Do you want to delete this record?');">Delete</a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="span8">
                                        <p></p>
                                        <p>

                                            <i class="icon-user"></i> by <a href="#">

                                                Me

                                            </a>
                                            | <i class="icon-calendar"></i> <?php echo $blog->created_on; ?>
                                            | <i class="icon-comment"></i> <a href="#">3 Comments</a>
                                            | <i class="icon-share"></i> <a href="#">39 Shares</a>
                                            | <i class="icon-tags"></i> Tags : <a href="#"><span class="label label-info">Snipp</span></a>
                                            <a href="#"><span class="label label-info">Bootstrap</span></a>
                                            <a href="#"><span class="label label-info">UI</span></a>
                                            <a href="#"><span class="label label-info">growth</span></a>
                                        </p>
                                    </div>
                                </div>
                                <div>
                                    <h3>Answers: </h3>
                                    <br>
                                    <?php if ($comment != null) : ?>
                                    <div class="comment_container">
                                    <?php foreach ($comment as $value) : ?>
                                        <div class="comment_container_inner clearfix">
                                            <div class="user_img_container">
                                                <?php if($value->user_image != null){ ?>
                                                    <img src="<?= base_url($value->user_image); ?>" alt="photo of <?php echo $value->username ?>" class="user_photo" title="<?php echo $value->username ?>">
                                                    
                                                <?php }else{ ?>

                                                <img src="<?= base_url('assets/uploads/default_profile.png'); ?>" alt="photo of <?php echo $value->username ?>" class="user_photo" title="<?php echo $value->username ?>">
                                                <?php } ?>  
                                            </div>
                                            <div class="coment_area">
                                                <span class="user_name">
                                                    <?php echo $value->username ?>
                                                </span>

                                                <span class="user_comment">
                                                    <?php echo $value->comment; ?>
                                                </span>
                                            <?php if ($this->session->userdata('user') == $value->c_user_id) { ?>
                                                <form action="<?= base_url('welcome/edit_comment_blog/') . $value->c_blog_id; ?>" method="post" id="update_form">
                                                    <input type="hidden" name="c_id" value="<?php echo $value->id; ?>">

                                                    <textarea name="update_comment" id="update_comment" class="update_comment" value="" rows="auto"></textarea>

                                                    <button class="btn btn-default btn-small pull-right" type="reset" onclick="hideUpdateForm(this)">Cancel</button>
                                                    <input type="submit" value="save" class="btn btn-default btn-small pull-right" style="margin-right:5px">
                                                </form>
                                                <?php 
                                            } ?>
                                            
                                            </div>

                                            <?php if ($this->session->userdata('user') == $value->c_user_id) { ?>
                                                <div class="edit_delete">
                                                    <a href="#" class="toPrevent" onclick="showUpdateForm(this)"><i class="fa fa-edit">
                                                    </i></a> 
                                                    <form action="<?= base_url('welcome/delete_comment_blog/') . $value->c_blog_id; ?>" method="post" class="delete_form">
                                                        <input type="hidden" name="post_id" value="<?= $value->id; ?>">
                                                        <button type="submit" class="toPrevent1" onclick="return confirm('Do you want to delete this record?');"><i class="fa fa-trash">
                                                    </i></button>
                                                    </form>
                                                    
                                                </div>
                                            <?php 
                                        } ?>
                                            

                                        </div>
                                    <?php endforeach; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div>
                                    <form method="post" action="<?php echo base_url('welcome/blog_comment'); ?>">
                                        <input type="hidden" name="c_blog_id" value="<?php echo $blog->id; ?>">
                                        <input type="hidden" name="c_user_id" value="<?php echo $this->session->userdata('user'); ?>">
                                        <textarea rows="4" cols="40" name="comment" placeholder="Comment here..."></textarea>
                                        <button type="submit" style="color: black;" class="btn btn-default">Comment</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr>
                        
                    </div>

                </div>
                
            </div>
            
        </div>

    </div>
</section>

<script src="<?= base_url('assets/js/comment.js')?>"></script>
