<br><br>
       <!--  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css"> -->
        <!--Section -->
        <section id="section" class="py-4 mb-4 bg-faded">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <a href="<?php echo base_url() . 'welcome/addpost'; ?>" class="btn btn-primary btn-block" >
                      <i class="fa fa-plus"></i>Add Post
                         </a>
                    </div>
                </div>
            </div>
        </section>

        <!--Posts-->
        <section id="posts">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="card-header">
                            <h4>Latest Posts</h4>

                            <div>
                                <?php foreach ($blogs as $posts): ?>
                                <div class="row">
                                    <div class="span8">
                                        <div class="row">
                                            <div class="span8">
                                                <h4><strong><a href="#"><?php echo $posts->title; ?></a></strong></h4>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="span2">
                                                <a href="#" class="thumbnail">
                                                    <img src="http://www.beshto.com/beshtoImages/photoShare/d535/u38841/u38841_700055_575149.jpg" alt="kodom ful">
                                                </a>
                                            </div>
                                            <div class="span6">
                                                <p>
                                                    <?php echo $posts->body; ?>
                                                </p>
                                                <p><a style="color: black;" class="btn btn-danger" href="<?php echo base_url('welcome/view/').$posts->id; ?>">Read more</a></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="span8">
                                                <p></p>
                                                <p>

                                                    <i class="icon-user"></i> by <a href="#">

                                                        <?php echo $posts->username; ?>

                                                    </a>
                                                    | <i class="icon-calendar"></i><?php echo $posts->created_on; ?>
                                                    | <i class="icon-comment"></i> <a href="#">3 Comments</a>
                                                    | <i class="icon-share"></i> <a href="#">39 Shares</a>
                                                    | <i class="icon-tags"></i> Tags : <a href="#"><span class="label label-info">Snipp</span></a>
                                                    <a href="#"><span class="label label-info">Bootstrap</span></a>
                                                    <a href="#"><span class="label label-info">UI</span></a>
                                                    <a href="#"><span class="label label-info">growth</span></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <?php endforeach; ?>
                            </div>

                        </div>
                        
                    </div>
                    <div class="col-md-3">
                        <div class="card card-primary mb-3 text-center text-white" style="background-color: purple;">
                            <div class="card-block">
                                <h3>No. Of Posts</h3>

                                <h1 class="display-4"><i class="fa fa-pencil"></i><?php echo $count_posts; ?></h1>

                                <a href="post.php" class="btn btn-sm btn-outline-secondary text white">View</a>
                            </div>
                        </div>

                        <div class="card card-warning mb-3 text-center text-white" style="background-color: black;">
                            <div class="card-block">
                                <h3>No. of Users</h3>
                                <h1 class="display-4"><i class="fa fa-pencil"></i><?php echo $count_users; ?></h1>
                                <a href="user.php" class="btn btn-sm btn-outline-secondary text white">View</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>


<br><br>
