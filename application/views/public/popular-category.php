
 <link rel="stylesheet" href="<?= base_url('assets/css/card.css') ?>">
<!-- Card Start-->

            <section id="popular-topic">

<div class="contact-header">
    <h2 class="contact-title wow shake">Which is best for you?</h2>
</div>
<div class="row">
    <div class="col-1-of-3 wow fadeInUp" data-wow-delay=".2s">
        <div class="card">
            <div class="cardf">
                <div class="card-picture">
                    &nbsp;
                </div>
                <h4 class="card-heading card-sci">
                    <span class="card-heading-span ">BASICS</span>
                </h4>
                <div class="card-details">

                    <p>How Skillsharing Can Help Your Career</p>

                    <ul>
                        <li>What Is Skillsharing?</li>
                        <li>The Benefits of Skillsharing</li>
                        <li>Skillsharing Online
                        </li>
                        <li>Community Skillsharing</li>
                        <li>How Much Does It Cost?</li>

                    </ul>
                </div>

            </div>
            <div class="cardb cardb__color1">
                <div class="card-cta">
                    <div class="card-price-box">
                        <p class="card-price-only">Your Choice</p>
                        <p class="card-price-value">Skillsharing</p>
                    </div>
                    <a href="skillsharing.html" class="btn btn-click">Click Here</a>
                </div>
            </div>


        </div>
    </div>


    <div class="col-1-of-3 wow fadeInUp" data-wow-delay=".4s">
        <div class="card">
            <div class="cardf">
                <div class="card-picture1">
                    &nbsp;
                </div>
                <h4 class="card-heading card-com">
                    <span class="card-heading-span ">BASICS</span>
                </h4>
                <div class="card-details">
                    <p>7 Best Jobs for Single Moms Starting Over</p>
                    <ul>
                        <li>Teacher</li>
                        <li> Healthcare Professional</li>
                        <li>Sales</li>
                        <li> Marketing Professional</li>
                        <li> Freelance From Home</li>


                    </ul>
                </div>

            </div>
            <div class="cardb cardb__color2">
                <div class="card-cta">
                    <div class="card-price-box">
                        <p class="card-price-only">Your Are</p>
                        <p class="card-price-value">Single Moms</p>
                    </div>
                    <a href="#popup" class="btn btn-click">Click Here</a>
                </div>
            </div>

        </div>
    </div>



    <div class="col-1-of-3 wow fadeInUp" data-wow-delay=".6s">
        <div class="card">
            <div class="cardf">
                <div class="card-picture2">
                    &nbsp;
                </div>
                <h4 class="card-heading card-arts">
                    <span class="card-heading-span ">BASICS</span>
                </h4>
                <div class="card-details">
                    <p>Top 15 Kids' Dream Jobs</p>
                    <ul>
                        <li> Dancer/Choreographer</li>
                        <li> Actor</li>
                        <li>Musician</li>
                        <li> Teacher</li>
                        <li>Scientist</li>

                    </ul>
                </div>

            </div>
            <div class="cardb cardb__color3">
                <div class="card-cta">
                    <div class="card-price-box">
                        <p class="card-price-only">Your Are</p>
                        <p class="card-price-value">Kids</p>
                    </div>
                    <a href="#popup" class="btn btn-click">Click Here</a>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="wow fadeInUp" data-wow-delay=".8s">
    <a href="tips.html" class="btn btn-discover">Discover All Career</a>
</div>

</section>

<!-- Card End-->