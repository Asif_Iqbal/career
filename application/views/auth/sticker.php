<link rel="stylesheet" href="<?= base_url('assets/css/login_register.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/utilities.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">



<div class="container category">
	<div class="card sign_up_card">
		<article class="card-body col-sm-6 m-auto">
			<h4 class="card-title mt-3 text-center">Add Sticker</h4>
			<form method="post" action="<?php echo base_url('admin/save_sticker'); ?>" >
				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input name="title" class="form-control fch" placeholder="title name" type="text">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input name="url" class="form-control fch" placeholder="url" >
				</div> <!-- form-group// -->


				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Insert</button>
				</div> <!-- form-group// -->    

				                                                                 
			</form>
		</article>
	</div> <!-- card.// -->
</div> 

