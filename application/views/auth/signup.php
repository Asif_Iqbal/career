
	<div class="card sign_up_card">
		<div class="home_background3">

		<article class="card-body1 col-sm-12">
			<h4 class="card-title mt-3 text-center">Create Account</h4>
			<form method="post" action="<?php echo base_url('auth/register'); ?>" enctype="multipart/form-data">
				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input name="username" class="form-control fch" placeholder="Full name" type="text">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
					</div>
					<input name="email" class="form-control fch" placeholder="Email address" type="email">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
					</div>
					<input class="form-control fch" placeholder="Create password" type="password" name="password">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
					</div>
					<input class="form-control fch" placeholder="Repeat password" type="password" name="c_password">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input type="file" name="user_image" class="form-control fch"  >
				</div> <!-- form-group// -->

				<select name="interested_field" class="custom-select1 select form-group">
	                <option selected> Select Interested Field</option>
	                <?php foreach($sub_category as $data): ?>
	                <option value="<?= $data->post_title; ?>"><?= $data->post_title; ?></option>
	                <?php endforeach; ?>
           		 </select> 

           		 <select name="user_field" class="custom-select1 select form-group">
	                <option selected> Select Your Field</option>
	                <?php foreach($cat_name as $data): ?>
	                <option value="<?= $data->cat_name; ?>"><?= $data->cat_name; ?></option>
	                <?php endforeach; ?>
           		 </select> 

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
				</div> <!-- form-group// -->    

				<p class="text-center">Have an account? <a href="<?= base_url('auth/login');?>">Log In</a> </p>                                                                 
			</form>
		</article>
	</div> <!-- card.// -->
</div> 
</div>

