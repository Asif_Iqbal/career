<link rel="stylesheet" href="<?= base_url('assets/css/login_register.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/utilities.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">



<div class="container category">
	<div class="card sign_up_card">
		<article class="card-body col-sm-6 m-auto">
			<h4 class="card-title mt-3 text-center">Add Sub Category</h4>
			<form method="post" action="<?= base_url('admin/save_sub_category'); ?>" enctype="multipart/form-data">
				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input name="sub_cat_name" class="form-control fch" placeholder="sub category name" type="text">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input name="sub_cat_heading" class="form-control fch" placeholder="sub category heading" type="text">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="sub category escription" type="text" name="sub_cat_description">
				</div> <!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input type="file" id="myFile" name="sub_cat_video_url">
                </div>
                

				<div class="form-group input-group group_mg_btm">
					<select id="employeeCombo" name="cat_name">
              	  		<option>Category</option>
           			 	<?php foreach($name as $result) : ?>
           	     		 <option value="<?php echo $result->cat_name; ?>"><?php echo $result->cat_name; ?></option>
          			  <?php endforeach; ?>
        			</select>
				</div>
                
                <!-- form-group// -->

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Insert</button>
				</div> <!-- form-group// -->    

				                                                                 
			</form>
		</article>
	</div> <!-- card.// -->
</div> 

