<!DOCTYPE html>
<html>
<head>
	<title>Search</title>
</head>
<body>
	

	<div class="hello container">
	
	<?php if($query != null){ ?>
		
			<?php foreach($query as $item){ ?>
			
			<br>
			<h1 class="wow fadeInDown title" data-wow-delay='.3s'><?= $item->post_heading ?></h1><br>
			<h3 class="wow fadeInUp des" data-wow-delay='.5s'><?= $item->post_description ?></h3><br>
			<b class="wow fadeInDown btn  btn_title" data-wow-delay='.6s'><a href="<?= base_url('welcome/posts/').$item->id; ?>">See more</a></b>
			
			<?php } ?>

		
	<?php }else{ ?>
		<h1 style="text-align: center;">
			<?php echo "No value"; ?>
		</h1>
	<?php } ?>
	</div>
</body>
</html>