<style>
#rating_modal .close{
    font-size: 40px;
    color: #fff;
    opacity: 1;
}

#rating_modal .modal-header{
    background: #d9534f;
    color: #fff;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
}   

.star-rating {
    line-height:32px;
    font-size:1.25em;       
}

.star {
    line-height:32px;
    font-size:1.25em;
    cursor: pointer;
}

.star-rating .fa-star{color: #c20001;}
.star .fa-star{color: #c20001;}
</style>






<div class="col-sm-12 profile_follow_editor">
    <div class="container p0">
        <div class="col-sm-12 profile_follow">
            <div class="col-sm-8 profile_padd">
                <div class="col-sm-9 p0">
                    <span class="pull-left pro-image-pull-left">
                        <img src="<?php echo $this->config->base_url();?>assets/frontend/images/profile_pic.png">
                    </span>
                    <span class="pull-left profile-pull-left">
                        <h2>STEVE GIULIANO</h2>
                        <p><span class="profile_padd_1">Locations:</span>Chicago, IL </p>
                        <p><span class="profile_padd_2">Website:</span><a class="" href="#">www.yourwebsite.com</a></p>
                        <p><span class="profile_padd_3">Twitter:</span><a class="" href="#">www.yourwebsite.com/tinurl</a></p>
                        <h3>BLOG</h3>
                    </span>
                    <div class="clear10"></div>
                    <div class="clear5"></div>
                    <a class="follow_btn" href="#">FOLLOW</a>
                    <div class="clearfix"></div>
                </div>
                <div class="col-sm-3 p0" id="following-data">
                    <ul class="follow_ul">
                        <li><a href="#" onclick="follower()"><h3>FOLLOWERS</h3> <span><?= $followers_count ?></span></li>
                        <li style="margin:0px;"><a href="#" onclick="following()"><h3>FOLLOWING</h3> <span><?= $follwing_count ?></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clear40"></div>
 <div class="container p0"> 
    <div class="col-sm-9 comment_main_padd">
          <div class="col-sm-12 p0">
            <div class="dropdown pull-right">
                <button style="text-align: left;" class="btn btn-primary dropdown-toggle picks_dropdown type="button" data-toggle="dropdown">Filter <span class="pull-right"><i class="fa fa-caret-down" aria-hidden="true"></i></span></button>
                <ul class="dropdown-menu">
                    <li>Picks Only</a></li>
                </ul>
            </div>
        </div>
        <div class="clear10"></div>
        <div class="clear5"></div>
        <div class="col-sm-12 profile_feedback">

            <span class="pull-right">
                <div class="star pointer" onclick="rating()" >
                <?php $rating_value = ceil($get_avg_rating[0]['average']); ?>
                    <span class="<?php if ($rating_value >= 1) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o';} ?>"></span>
                    <span class="<?php if ($rating_value >=2) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>" ></span>
                    <span class="<?php if ($rating_value >=3) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o';} ?>"></span>
                    <span class="<?php if ($rating_value >=4) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>" ></span>
                    <span class="<?php if ($rating_value >=5) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>"></span>
                    <input type="hidden" name="whatever1" class="rating-value" value="0">
                </div>
                <!-- End Rating Picks -->
            </span>
            <span class="pull-left feedback-img-pull-left"><img src="<?php echo $this->config->base_url();?>assets/frontend/images/comment_img.png"></span>
            <span class="pull-left user-pull-left">
                <h3>JOHN DOE &nbsp;<span>ADDED PICK:</span></h3>
                <h4>TCU-6 @ TEXAS TECH</h4>
                <h5>11/18/2017 3:10PM</h5>
            </span>
            <div class="clearfix"></div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

              <p class="collapse" id="block-id">
               Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>

  <!-- aria-expanded attribute is mandatory -->
  <!-- bootstrap changes it to true/false on toggle -->
  <a href="#block-id"
     class=""
     data-toggle="collapse"
     aria-expanded="false"
     aria-controls="block-id">

    <span class="collapsed">
      Read more
    </span>
    <span class="expanded">
      Read Less
    </span>

  </a>
  <input type="hidden" name="post_id" id="post_id" value="1">

        </div>
    </div>
</div>  
<div class="modal fade" id="rating_modal" role="dialog" tabindex="-1" aria-hidden="true" aria-labelledby="myModalLabel">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <button type="button" onclick="javascript:window.location.reload()" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Pick Rating</h3>
    </div>
    <div class="modal-body form">

    <div class="row">
        <div class="col-md-12">
            <h3 class="modal-title star-rating">
                Rating &nbsp; &nbsp; 
                <span class="fa fa-star-o" data-rating="1"></span>
                <span class="fa fa-star-o" data-rating="2"></span>
                <span class="fa fa-star-o" data-rating="3"></span>
                <span class="fa fa-star-o" data-rating="4"></span>
                <span class="fa fa-star-o" data-rating="5"></span>
                <input type="hidden" name="whatever1" class="rating-value" value="2.56">
           </h3>
           <p id="demo"></p>
            <!-- Rating Picks -->       
            <!-- <div class="star-rating">

            </div> -->
            <!-- End Rating Picks -->
        </div>
        <div class="clear10"></div>
        <div class="col-md-12">
            <textarea class="form-control" id="comment_rating" placeholder="Description"></textarea>
            <br>
            <button class="btn btn-danger pull-right" onclick="post_rating()"> Post Rating</button>
        </div>

    </div>

    <?php //echo "<pre>"; print_r($rating_data); ?>
    <?php foreach ($rating_data as $r_data) { ?>

    <div class="row">
        <div class="col-md-12">
            <h3 class="modal-title star"><?php echo $r_data->fname; ?>
                 &nbsp; &nbsp; 
                <span class="<?php if ($r_data->rating >= 1) {
                    echo 'fa fa-star';
                }else{ echo 'fa fa-star-o';} ?>"></span>
                <span class="<?php if ($r_data->rating >=2) {
                    echo 'fa fa-star';
                }else{ echo 'fa fa-star-o'; } ?>" ></span>
                <span class="<?php if ($r_data->rating >=3) {
                    echo 'fa fa-star';
                }else{ echo 'fa fa-star-o';} ?>"></span>
                <span class="<?php if ($r_data->rating >=4) {
                    echo 'fa fa-star';
                }else{ echo 'fa fa-star-o'; } ?>" ></span>
                <span class="<?php if ($r_data->rating >=5) {
                    echo 'fa fa-star';
                }else{ echo 'fa fa-star-o'; } ?>"></span>
                <input type="hidden" name="whatever1" class="rating-value" value="2.56">
            </h3>
            <!-- Rating Picks -->       
            <!-- <div class="star">

            </div> -->
            <!-- End Rating Picks -->
        </div>

        <div class="col-md-12">
            <?php //echo "<pre>"; print_r($all_rating);  ?>
            <span><?php echo $r_data->comment_rating; ?></span>
        </div>

         <div class="clear10"></div>

    </div>
    <?php }  ?>

    </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function rating() {
    $('#rating_modal').modal('show');
} </script>


<script>
function post_rating() {
    var post_id = $('#post_id').val();
    var rating = $('#rating').val();
    var starcount = $star_rating.siblings('input.rating-value').val();
    if (starcount&&comment_rating) {
    $.ajax({
        url: "main/rating",
        data: {starcount: starcount, blogid: blogid, comment_rating: comment_rating},
        type: "POST",
        success: function (data) {

            location.reload();

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('error in insert rating data');
        }
    });
    }else{
        alert("Please rate and comment");
    }

}
var $star_rating = $('.star-rating .fa');
var SetRatingStar = function() {
    return $star_rating.each(function() {
    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {

    return $(this).removeClass('fa-star-o').addClass('fa-star');
    } else {
        return $(this).removeClass('fa-star').addClass('fa-star-o');
    }
});

};
// alert($star_rating.siblings('input.rating-value').val());

$star_rating.on('click', function() {
    $star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
});
</script>