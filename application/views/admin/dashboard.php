   
  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
      
        <div class="row">
            <div class="col-lg-12">
                <table id="example" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>User Level</th>
                            <th>Created_on</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($user as $detail): ?>
                        <tr>

                            <td><?php echo $detail->id; ?></td>
                            <td><?php echo $detail->username; ?></td>
                            <td><?php echo $detail->email; ?></td>
                            <td><?php echo "invalidFormat"; ?></td>
                            <td><?php echo $detail->user_level; ?></td>
                            <td><?php echo $detail->created_on; ?></td>
                            <td><a href="<?php echo base_url('admin/delete/').$detail->id; ?>" class="btn btn-danger" onclick="return confirm('Do you want to delete this user?');">Delete</a></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->