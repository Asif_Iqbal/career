<?php

	class Migration_Add_User_Cv extends CI_Migration{

		public function up(){
			$this->load->dbforge();

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'user_id' => array(
						'type' => 'INT',
						'constraint' => '100'
					),
					'user_image' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'username'=>array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'email' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'number' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'preferred_job' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'father' => array(
						'type' => 'VARCHAR',
						'constraint' => '33'
					),
					'mother' => array(
						'type' => 'VARCHAR',
						'constraint' => '33'
					),
					'date_of_birth' => array(
						'type' => 'VARCHAR',
						'constraint' => '15'
					),
					'gender' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					'marital_status' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					'nationality' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'religion' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					'permanent_address' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'current_location' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'interested_fields' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'training_title' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'training_topic' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'training_institute' => array(
						'type' => 'VARCHAR',
						'constraint' => '55'
					),
					'training_country' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'training_location' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'training_year' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'training_duration' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'employment_duration' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'employment_position' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'employment_company' => array(
						'type' => 'VARCHAR',
						'constraint' => '33'
					),
					'employment_address' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'employment_department' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					's_major' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					's_institute' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					's_result' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					's_p_year' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					's_duration' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'c_major' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					'c_institute' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'c_result' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					'c_p_year' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					'c_duration' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'u_major' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'u_institute' => array(
						'type' => 'VARCHAR',
						'constraint' => '33'
					),
					'u_result' => array(
						'type' => 'VARCHAR',
						'constraint' => '11'
					),
					'u_p_year' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					),
					'u_duration' => array(
						'type' => 'VARCHAR',
						'constraint' => '22'
					)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('users_cv');

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'u_id'=>array(
						'type' => 'INT',
						'constraint' => '100'
					),
					'msg' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'date' => array(
						'type' => 'TIMESTAMP'
					),
					'status' => array(
						'type' => 'INT',
						'constraint' => '100'
					)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('tbl_msg');

		}

		public function down(){
			$this->load->dbforge();
			$this->dbforge->drop_table('users_cv');
			$this->dbforge->drop_table('tbl_msg');
		}
	}

?>