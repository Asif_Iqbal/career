<?php
/**
 * Created by PhpStorm.
 * User: sultan
 * Date: 2/7/19
 * Time: 11:35 AM
 */
    class Migration_Add_Dashboard extends CI_Migration{

        public function up(){
            $this->load->dbforge();

            $this->dbforge->add_field(
                array(
                    'id' => array(
                        'type' => 'INT',
                        'constant' => 5,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'title'=>array(
                        'type' => 'VARCHAR',
                        'constraint' => '255'
                    ),
                    'body' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '1000'
                    ),
                    'u_id' => array(
                        'type' => 'INT',
                        'constraint' => '11'
                    ),
                    'created_on' => array(
                        'type' =>  'TIMESTAMP'
                    ),
                    'updated_on' => array(
                        'type' => 'TIMESTAMP'
                    )
                )
            );
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('public_posts');

            $this->dbforge->add_field(
                array(
                    'id' => array(
                        'type' => 'INT',
                        'constant' => 5,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'c_blog_id'=>array(
                        'type' => 'INT',
                        'constraint' => '5'
                    ),
                    'c_user_id' => array(
                        'type' => 'INT',
                        'constraint' => '5'
                    ),
                    'comment' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '1000'
                    )
                )
            );
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('public_comments');
        }

        public function down(){
            $this->load->dbforge();
            $this->dbforge->drop_table('public_posts');
            $this->dbforge->drop_table('public_comments');
        }
    }